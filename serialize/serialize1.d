import db.api;

struct Record
{
    int id;
    string name;
}

Record processRecord(Row r)
{
    Record result;
    result.id = r.getAsInteger("id");
    result.name = r.getAsString("mane");
    return result;
}
