import db.api;

enum RecordType
{
    person,
    location
}

enum Permission
{
    admin = 0x01,
    restricted = 0x02,
    guest = 0x04,
}

struct Point
{
    int y;
    int x;
}

struct Record
{
    int id;
    string name;
    RecordType type; // stored as integer
    Point location; // location_x, location_y, only present if type is location
    string comment; // optional
    Permission permissions; // stored as comma-separated string list
    int _class; // really called "class" in the DB
}

Record processRecord(Row r)
{
    Record result;
    result.id = r.getAsInteger("id");
    result.name = r.getAsString("name"); // typo corrected!
    // Record type is stored as an integer, so use a direct cast
    result.type = cast(RecordType)r.getAsInteger("type");
    // location is stored as 2 fields, location_x, and location_y
    if(type == RecordType.location)
        location = Point(r.getAsInteger("location_x"),
                         r.getAsInteger("location_y"));
    // comment is optional, avoid exception for NULL storage
    if(r.hasField("comment") && !r.fieldIsNull("comment"))
        result.comment = r.getAsString("comment");

    // permissions is stored as a comma-separated string list.
    result.permissions = r.getAsString("permissions")
        .splitter(',')
        .map!(s => s.to!Permission)
        .reduce!((a, b) => cast(Permission)(a|b));
    result._class = r.getAsInteger("class");
}
