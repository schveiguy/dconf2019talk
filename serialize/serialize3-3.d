import db.api;

enum RecordType
{
    person,
    location
}

enum Permission
{
    admin = 0x01,
    restricted = 0x02,
    guest = 0x04,
}

struct Point
{
    int y;
    int x;
}

struct Record
{
    int id;
    string name;
    RecordType type; // stored as integer
    Point location; // location_x, location_y, only present if type is location
    string comment; // optional
    ...
}

Record processRecord(Row r)
{
    ...
    if(type == RecordType.location)
        location = Point(r.getAsInteger("location_x"),
                         r.getAsInteger("location_y"));
    // comment is optional, avoid exception for NULL storage
    if(!r.fieldIsNull("comment"))
        result.comment = r.getAsString("comment");

    // permissions is stored as a comma-separated string list.
    result.permissions = r.getAsString("permissions")
        .splitter(',')
        .map!(s => s.to!Permission)
        .reduce!((a, b) => cast(Permission)(a|b));
    result._class = r.getAsInteger("class");
}
