import db.api;

enum RecordType
{
    person,
    location
}

enum Permission
{
    admin = 0x01,
    restricted = 0x02,
    guest = 0x04,
}

struct Point
{
    int y;
    int x;
}

struct Record
{
    int id;
    string name;
    RecordType type; // stored as integer
    Point location; // location_x, location_y, only present if type is location
    string comment; // optional
    Permission permissions; // stored as comma-separated string list
    int _class; // really called "class" in the DB
}

Record processRecord(Row r)
{
    ...
    // permissions is stored as a comma-separated string list.
    result.permissions = r.getAsString("permissions")
        .splitter(',')
        .map!(s => s.to!Permission)
        .reduce!((a, b) => cast(Permission)(a|b));
    result._class = r.getAsInteger("class");
}
