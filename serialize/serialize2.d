import db.api;

struct Record
{
    int id;
    string name;
}

Record processRecord(Row r)
{
   Record result;
   import std.traits;
   static foreach(i, m; FieldNameTuple!Record)
   {
      __traits(getMember, result, m) =
          r.getWithType!(typeof(__traits(getMember, result, m)))(m);
   }
}
