struct Record
{
    int id;
    string name;

    @dbType!int
    RecordType type;

    Point location; // stored as location_x, location_y, only present if type is location
    string comment; // optional
    Permission permissions; // stored as comma-separated string list
    int _class; // really called "class" in the DB
}
