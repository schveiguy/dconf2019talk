struct Record
{
    int id;
    string name;

    @dbType!int
    RecordType type;

    @dbOnlyIf!(R => r.type == RecordType.location)
    Point location;

    string comment; // optional
    Permission permissions; // stored as comma-separated string list
    int _class; // really called "class" in the DB
}
