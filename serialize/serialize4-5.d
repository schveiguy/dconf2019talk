struct Record
{
    int id;
    string name;

    @dbType!int
    RecordType type;

    @dbOnlyIf!(R => r.type == RecordType.location)
    Point location;

    @dbOptional
    string comment;

    @dbProcessWith!(parseCommaList)
    Permission permissions;

    int _class; // really called "class" in the DB
}
