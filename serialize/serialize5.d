Record processRecord(Row r)
{
    import std.traits;

    Record result;
    foreach(m; __traits(allMembers, Record))
    {
        static if(hasUDA!(__traits(getMember, Record, m), DbOptional))
        {
            if(!r.hasField(m) || r.fieldIsNull(m))
                continue;
        }
        static if(hasUDA!(__traits(getMember, Record, m), DbOnlyIf))
        {
            if(!getUDA!(__traits(getMember, Record, m), DbOnlyIf).pred(result))
                continue;
        }
        static if(hasUDA!(__traits(getMember, Record, m), DbProcessWith))
        {
            __traits(getMember, Record, m) =
                getUDA!(__traits(getMember, Record, m), DbProcessWith).processor(r))
        }
        else
        {
            alias mType = typeof(__traits(getMember, Record, m));
            static if(hasUDA!(__traits(getMember, Record, m), DbType))
                alias TypeToRead =
                    getUDA!(__traits(getMember, Record, m), DbType).Type;
            else
                alias TypeToRead = mType;
            static if(hasUDA!(__traits(getMember, Record, m), DbName))
                enum fieldName =
                    getUDA!(__traits(getMember, Record, m), DbName).name;
            else
                enum fieldName = m;
            __traits(getMember, Record, m) = cast(mType)
                r.getWithType!TypeToRead(fieldName);
        }
    }

    return result;
}

