# D is for (De)Serialization

## Part 1: You have a computer, why not use it?

In the fall of 2016, our HR director gave her notice. I was put in charge of Payroll for a few months. Will go into details about what was needed for this, and how it was all done on paper. The moral of the story is, "you have a computer why not use it?"

[Note: The point of this part to get a few laughs, and relate the power of D over other languages to the evolution of processes from paper to computer.]

The fundamental advice of "You have a computer, why not use it?" I have been heeding or preaching for a long time. Any time I see someone doing by hand what can be done on a computer, I try to intervene. People shouldn't waste their days doing something that a computer is really really good at doing!

In the end, I turned what was a 3-4 day process into a 4 hour process.

(This section should last about 5-10 minutes.)

## Part 2: You have a compiler, why not use it?

(20 minutes)

Let's turn to D. I believe with D, we have another fundamental shift in what is possible and easy to accomplish in terms of Generative Programming. Just like the advice "You have a computer, why not use it," we can change that into "You have a compiler, why not use it?"

Take for instance a simple struct:

```D
struct Record 
{
   int id;
   string name;
}
```

Now, suppose this struct matches rows of a database table. How would we write a function that converts a non-typed row of data from the database into one of these structs?

I'm going to write a simple function that does this, which is given a generic "Row" type.

```D
Record processRecord(Row r)
{
   Record result;
   result.id = r.getAsInteger("id");
   result.name = r.getAsString("mane");
}
```

This seems simple enough, but there is something incorrect, did anyone spot it?

I put "mane" instead of "name". It compiled, but when run, it doesn't get the data. Depending on the database mechanism, this either errors out, or doesn't actually get the data requested. Usually this is a pretty easy fix, but annoyingly, notice how we had to write "name" twice -- once for the field name, and once for the Record member name. Same thing for "id". We'll get to this later.

Let's introduce the workhorses of D generative programming -- `static foreach` and `__traits`.

Instead of manually looking at the structure we are trying to deserialize, we can let the compiler do it:

```D
Record processRecord(Row r)
{
   Record result;
   static foreach(i, m; __traits(allMembers, Record))
   {
      __traits(getMember, result, m) = r.getWithType!(typeof(__traits(getMember, result, m)))(m);
   }
}
```

What this does is exactly what you and I would do when looking at a structure -- go through the members one at a time, figure out the type of that member and the name of that member, and call the appropriate function. But instead of our brains' poor ability to copy and paste things, the compiler can do this perfectly well without any mistakes. Boilerplate is now handled, and with more accuracy.

The reason I think D is so natural for writing generative code is due to this mechanism. Because it reads just like you would approach it, it's easier to understand.

The only thing I don't like about this code is the ugliness of `__traits(getMember`. But it's the most basic thing you can do without writing too much wrapper code. It also has special meaning that is lost if we wrap it. It's like you wrote the member access `result.member` directly. Things like attributes or other special things may get lost if we wrap that into a function.

What if that database record and struct increase in complexity a bit?

```D
enum RecordType
{
   person,
   location
}

enum Permission
{
   admin = 0x01,
   restricted = 0x02,
   guest = 0x04,
}

struct Point
{
   int y;
   int x;
}

struct Record
{
   int id;
   string name;
   RecordType type; // stored as integer
   Point location; // stored as location_x, location_y, only present if type is location
   string comment; // optional
   Permission permissions; // stored as comma-separated string list
   int _class; // really called "class" in the DB
}
```

So how does our manual function look with these updates?


```D
Record processRecord(Row r)
{
   Record result;
   result.id = r.getAsInteger("id");
   result.name = r.getAsString("name"); // typo corrected!
   // Record type is stored as an integer, so use a direct cast
   result.type = cast(RecordType)r.getAsInteger("type");
   // location is stored as 2 fields, location_x, and location_y
   if(type == RecordType.location)
       location = Point(r.getAsInteger("location_x"), r.getAsInteger("location_y"));
   result.name = r.getAsString("name"); // typo corrected!
   // comment is optional, avoid exception for NULL storage
   if(!r.fieldIsNull("comment"))
      result.comment = r.getAsString("comment");

   // permissions is stored as a comma-separated string list.
   result.permissions = r.getAsString("permissions")
             .splitter(',')
             .map!(s => s.to!Permission)
             .reduce!((a, b) => cast(Permission)(a|b));
   result._class = r.getAsInteger("class");
}
```

So the function is a little more complicated, but pretty straightforward. Notice how we are reacting to the comments in the record itself. There's also a bug in this code, but it's a bit more subtle, and will likely cause data corruption more than a runtime error. Anyone spot it?

Now that we've gone through this, who's ready to write the next one!? What if there are 20 different such structs, each with instructive comments for certain members? The answer is nobody. This is boilerplate. This is boring. This is tedious. This leads to subtle errors and mistakes.

In order to remedy some of this "custom" boilerplate, we use another great feature for generative programming -- the UDA. Rather than being a mechanism to *generate* code like static foreach and `__traits`, it is a mechanism to *describe* how we should handle the data. What if we replaced all our comments in the structure with UDAs? We get the same effect, but the compiler can actually react to them:

```D
struct Record
{
   int id;
   string name;

   @dbType!int
   RecordType type;

   @(dbOnlyIf!(R => r.type == RecordType.location))
   Point location;

   @dbOptional
   string comment;

   @dbProcessWith!(parseCommaList)
   Permission permissions;

   @dbName("class")
   int _class;
}
```

Now, with our generative function, we can do some more clever things based on the attributes, and because we do this in a static loop, each member gets the same treatment. Want a new behavior? Attach an attribute that effects the behavior. You can be as complicated or simple as you want -- it's your boilerplate, your code, your style of writing.

```D
Record processRecord(Row r)
{
   foreach(m; __traits(allMembers, Record))
   {
      static if(hasUDA!(__traits(getMember, Record, m), optional))
      {
          if(r.fieldIsNull(m))
              continue;
      }
      static if(hasUDA!(__traits(getMember, Record, m), DbType))
         alias TypeToRead = getUDA!(__traits(getMember, Record, m), DbType).Type;
      else
         alias TypeToRead = typeof(__traits(getMember, Record, m));
      ... // todo: fill this out
   }
}
```

Note how I handle each attribute individually, and I can develop almost a script for the compiler to follow to determine how best to read each item. Just like I develop a program to handle different options at runtime, I can use static if, static foreach, and traits to build a program with the correct options at compile time. I have my D compiler, so I'm using it.

At the end, we have something that not only builds a Record, but that can be provided with ANY custom type, and properly builds that type because it has been annotated where the custom items are used. In other words, we write comments to the compiler on how we would like it to proceed, and it implements the correct and most efficient function possible. And we do this simply by instantiating a function template.

This is way way better than boilerplate, or using external tools to generate code. Generating code right in the compiler has the access to everything the compiler knows, which is quite a lot! When your structure changes, the code changes. There are no stale files to deal with, or other tools to update.

## Part 3: Why is D better?
(5-10 minutes)

Let's look at some other languages, and how they tackle serialization compared to D.

### Java: Serializable

Java does everything for serialization via reflection. For the most part. How it works is, you implement the no-method interface java.io.Serializable. That's it. Then the serialization can happen.

But why? In fact, because Java serialization is somewhat of a kludgy @system call. It uses reflection to access the private data, and constructs the object on the other side without calling a constructor. In essence, Java says "I won't touch your private parts, unless you give me permission. And then I will read and write all over them."

Because of the runtime penalty (though JIT alleviates this somewhat), this is not as desirable as D. Instead of having the compiler write the code that we would write (generative), we have to follow the rules of the runtime, and only use what it gives us (reflection). Writing code just using reflection can be painful and really difficult to debug or read.

In actuality, java native serialization is somewhat frowned upon, and most people use hand-written protocol constructors to serialize and deserialize data, like Protocol Buffers, or XML.

### Dynamic typed languages like PHP and Python

Dynamically typed languages really have no choice -- there is no definition of what a type is anyway. Everything is a variant, and RTTI is used for reading/writing objects.

Similar to Java, the runtime execution doesn't really allow too much for optimization based on the type of data. And effectively, D can do the same thing but just faster.

Really the only benefit of using RTTI vs generative code is that RTTI is going to be a smaller piece of code.

But D can even mimic this -- think of a JSON parser which parses into a JSON representation type.

### C/C++: roll-your-own

While C++ gives us some generative capabilities, it still doesn't handle everything for us. With the boost::archive library, if your class/struct defines the function `serialize`, which accepts a template `Archive` type, you then use the `&` operator to push or pull all your data. It's clever in that it allows you to write one function for both serialization and deserialization. It's nice in that the archive type can define archive-specific ways to do things.

In addition, it's not required that the archiving function be a member, you could add it later as a standalone function -- just like operators. However, make sure it's a friend if you need to pass private data!

The problem, though, is that it's somewhat limited. For instance, only the data is passed to the serializer, not the name. This means that data ordering is required to be identical, and format changes can wreak havoc.

But what about archive formats like XML, which require a name for elements?  boost::archive has a solution to that! You just use this handy-dandy macro `BOOST_SERIALIZATION_NVP`. This passes the name of the item along with the item itself. For archive formats that do not need it, the name is ignored. What could be simpler? (hint: use D instead)

How about C? 

C has no shortage of serialization libraries, but I couldn't find one that did anything close to what D is capable of. All of them abstract the serialization parts, but you must still write all the function calls in your hand-written function. I imagine the C preprocessor could provide a way to create a serialization function automatically, but one would have to resort to using macros to declare all the parts of your type. In this area, D is far more attractive.

## Part 4: JSON with iopipe

(5 minutes)

2 years ago, I showcased my iopipe library, with a simple yet effective and efficient JSON parser. This winter I have worked on taking it to the next level -- JSON serialization of D types.

Using the parser, I wrote a deserializer first. Everything is based on overloads -- all the primitive types are really simple. It's the arrays and structs which are the most interesting.

But using generative code, I can easily deserialize ANY struct without problems. I even support UDAs (added as needed) to affect the behavior.

(I plan to show the code during this, but don't have it set up here).

The nice thing here, and this is really a feature of iopipe more than D, is that there is no intermediate representation -- your type IS the deserialized version. I hope soon to have this ready for release, and to measure it against other JSON serialization libraries.

## Part 5: How about some vibe.d magic?

(10 minutes)

Let's take a look at a pretty straightforward class which is used in vibe.d to handle a set of related routes.

```D
```

Note: I'm going to display quite a few of the features of vibe.d here. I haven't fleshed it out yet, but will cover dependency injection, and the way vibe.d maps http requests to function calls in your class by introspecting everything about your class.

## Part 6: Powering devices

(2-5 minutes)

In 2014, one of my favorite talks was from Michael V. Franklin, and how he spent a few months trying to get D to run on his ARM-M Cortex processor. In one part of this, he wrote a C# program to process the data from the chip spec pdf to generate D code that would properly deal with the register in the most efficient way possible, with compile-time enforcement of the register rules. Testing his generated code vs. the chip-manufacturer provided C library, he was able to twiddle GPIOs about 6.5 times faster.

Think about that -- a chip company who's goal when it comes to providing support libraries is to provide the most effective and efficient library for their microprocessors was soundly beaten by a guy just "trying out D". Not only that, but this was done in a way that could be used to handle ALL of the registers in the 1700-page spec, assuming they all can be parsed.

But even though I have massive kudos to Mike for his work, I imagine that today he may forgo his use of C#, and use the compiler directly. Almost literally, you could write a D program that accepts as input *to the compiler* of a PDF file that describes chip registers, and have it create the best possible code for all of those registers without mistakes (assuming the spec is correct) and without any manual effort at all. Because these specs are written by the same group of people, and in the same fashion, working with different chips could be as easy as instantiating with a different PDF.

As someone who has written code for microprocessors, and dealt with bitbanging a protocol using GPIOs, this looks SUPER attractive for that space. Generative code here could be revolutionary when it comes to register libraries and multiple chip support.

## Part 7: Drawbacks and conclusion.

(5 minutes)

Compile times with generative code can suck. A lot. Take a vibe.d default program. Building this takes my 2014 macbook pro 4.5 seconds (without pinging registries, or building any dependencies). Add a simple diet template, which essentially is parsing a file at compile time and transcoding it into D code, and you add 300ms. Any time you use a lot of generative features, the cost can add up. In this layman's experience, using lots of templates (especially recursive ones) can add some measurable amount of compile time. But the worst offender that I've experienced is with CTFE. The compiler just isn't great at executing runtime code in the interpreter.

If there is any significant drawback that can provoke someone to avoid generative code like this, it's compile-time. D is super-attractive because of the reputation for fast compile times. This is an area that we should strive to improve on more than anything else.

As we hope and pray every day for newCTFE to be ready for prime time, we are a bit stuck. On one hand, D has incredible generative features that are too good to ignore, but dispite the amazing power, the cumulative results of using more and more features is going to be a drag on productivity. Hopefully this tradeoff will be a thing of the past at some point.

But generative programming IS the main draw of D. Anything and I mean ANYTHING we can do to promote or improve the usage of generative programming in D should be high priority. It is the crown jewel of this language, and we should make sure anything that should be possible is possible.
