# @color[red](D) is for (De)serialization
### Steven Schveighoffer

Note:
Great to be back at dconf AND to London. Honeymoon was almost 17 years ago.
Enough chit-chat, it's time to talk about what everyone is eagerly waiting for... 

---

# Payroll!!!

@ul

- In the fall of 2016, our HR manager gave her notice.
- Needed someone to handle Payroll
- Who could do such a task?

@ulend

---

### Payroll man!

@img[clean-img span-40](assets/img/payrollman1.png)

@snap[west span-20]
@box[bg-gold text-white rounded box-padding fragment](2 + 2 = 4)
@snapend

@snap[east span-35]
@img[clean-img fragment](assets/img/basic-flowchart.jpg)
@snapend

Note:
- He was good at math
- He was a master at processes
- And he was quite the looker (switch slide!)

---

### Payroll man!

@img[clean-img span-40](assets/img/payrollman2.png)

@snap[west span-20]
@box[bg-gold text-white rounded box-padding](2 + 2 = 4)
@snapend

@snap[east span-35]
@img[clean-img](assets/img/basic-flowchart.jpg)
@snapend

Note:
That's right friends, I am payroll man. I was chosen to run payroll in the HR director's absence. Now that the company had it's victim... I mean volunteer, let's see what steps are involved in running payroll.

---
# Steps for Payroll

---

## 1. Payroll Timesheet

@ul
- Print one at the start of every payroll
- Sorted intuitively by department
- Record hours
- Record changes to status
@ulend

Note:
"Intuitively by department" because who orders things by name? Linear search all the way!

---

@img[clean-img span-80](assets/img/payroll_worksheet.png)

Note:

The names have been covered to protect the innocent. Note all the hand written notes in here.

---

### 2. Individual Timesheets

@ul
- Time started and stopped each task
- Hours for each task
- Total hours for day
- Drive time (am/pm)
- Total hours for week (regular and overtime)
- Overnights
@ulend

Note:

Why am I showing you all this? Because most of these are redundant information, all I really need is the start and stop time for each task. Each extra piece is another place for someone to screw up.
---

@img[clean-img span-80](assets/img/timesheet-1.jpg)
---

@img[clean-img span-80](assets/img/timesheet-2.jpg)

---
### 3. Punchcards

@ul
- Office employees use RFID punchcards
- But must also manually write them down
- Shore up punch card log with written data
@ulend

---

### 5. Enter data

@ul

- Copy all data from the payroll timesheet to the web site
- Double check all numbers by adding on a calculator!
- Adjust anything that needs adjusting (salary, tax witholdings, etc)

@ulend

---

### 6. Other stuff

@ul
- Commissions
- Time off report
- Run post-payroll reports
- Print everything, put in a folder
@ulend

---

### 6. Other stuff
@ul
- Separate folder for each PW job
- Extract 401k report from payroll system
- Update 401k CSV file... BY HAND
- 401k loan updates
@ulend

---

### 6. Oh God it keeps going
@ul
- Print 401k report for separate folder
- Send commission sheets to salespeople
- Certified payroll
- Print out timesheet for next payroll
@ulend

---

### Time needed to prepare payroll

@box[bg-orange text-white fragment](3-4 days)

### @css[fragment](Time available to prepare payroll)

@box[bg-gray text-red fragment](2 days)

---

# The Moral

@css[fragment]("You have a computer, why not use it?")
<div>
@css[fragment](Use computers for everything they are good at)
</div>

---

@snap[north span-100]
## Timecard App
@snapend

@snap[west span-50]
@img[clean-img span-50](assets/img/tcapp1.png)
@snapend

@snap[midpoint span-50]
@img[clean-img span-50](assets/img/tcapp2.png)
@snapend

@snap[east span-30 text-08]
@ul
- No more time math
- Always have it with them
- Links to jobs in our tracking system
- Replace unreliable punchcard system
@ulend
@snapend
---

## Payroll Timesheet

@ul
- Use Excel to track everything instead of paper
- VBA macro to generate CSV for upload to payroll system
- No more clunky transfer, one button push
- All totals calculated instantly.
@ulend

Note:
Robert is going to hate me for this

---

## Time to run payroll now?

@box[bg-green fragment text-20](4 *hours*)

---

# D Serialization

@css[fragment](You have a computer, why not use it?)

Note:

I feel that D is the same kind of revolution over normal programming languages as computers are over paper.

---

# D Serialization

You have a ~~computer~~ @color[red](D Compiler), why not use it?

---

A Simple Example

@code[d](serialize/serialize1.d)
 
@[3-7]()
@[9-11](Initialize Record)
@[5,12](Parse integer field)
@[6,13](Parse string field)
@[14](Return)
@[1-15](Does it work?)

---

# A Generative Approach
@ul
- `static foreach`
- `__traits`/`std.traits`
- `static if`
@ulend

Note:

Here are the 3 major weapons that D has for generating code

---

A Generative Approach

@code[d zoom-07](serialize/serialize2.d)

@[3-7, zoom-12](Same definition)
@[9-11, zoom-12](Initialize Record)
@[12, zoom-12](Import some generative magic)
@[13-14,17, zoom-12](Get all the field member names)
@[15, zoom-12](Assign the field to...)
@[15-16, zoom-12](The exact conversion expected)
@[9-18, zoom-12](The whole function)

---
A More complicated case

@code[d zoom-07](serialize/serialize3.d)

@[3-14](Enums and Types)
@[16-20](Enums and Types)
@[22-31](More complex, more convenient)
@[33-35](Declare the result)
@[24-25,36-37](Same as before)
@[26,38-39](Need to parse this as an integer)

---
A More complicated case

@code[d zoom-07 code-reveal-fast](serialize/serialize3-2.d)

@[27,36-39](Parse as location_x, location_y)

---
A More complicated case

@code[d zoom-07 code-reveal-fast](serialize/serialize3-3.d)

@[28,38-40](Only store comment if present)

---
A More complicated case

@code[d zoom-07 code-reveal-fast](serialize/serialize3-4.d)

@[29,36-40](Comma-seperated map parsing)

---

A More complicated case

@code[d zoom-07 code-reveal-fast](serialize/serialize3-4.d)

@[30,41](avoid keyword collision)

---

@code[d zoom-07 code-reveal-fast](serialize/serialize3.d)

@[38-53](Can you spot the bug?)

---

@code[d zoom-07 code-reveal-fast](serialize/serialize3-5.d)

@[16-20,27-30](How about now?)

---

# More generative approach

@ul
- User Data Attributes (UDAs) instead of comments
- Compiler can react to them, just like we react to the comments.
@ulend

---

Replace comments with UDAs
@code[d](serialize/serialize4-1.d)

@[5]()
---

Replace comments with UDAs
@code[d code-reveal-fast](serialize/serialize4-2.d)

@[6-7](You can pass types to UDAs)
@[9]()

---

Replace comments with UDAs
@code[d code-reveal-fast](serialize/serialize4-3.d)

@[9-10](And lambdas)
@[12]()

---

Replace comments with UDAs
@code[d code-reveal-fast](serialize/serialize4-4.d)

@[12-13](Simple tag)
@[15]()

---

Replace comments with UDAs
@code[d code-reveal-fast](serialize/serialize4-5.d)

@[15-16](Pass a normal function)
@[18]()

---

Replace comments with UDAs
@code[d code-reveal-fast](serialize/serialize4-final.d)

@[18-19](and so on...)
@[1-20, zoom-06](The result)

---

Generative serialization

@code[d zoom-07](serialize/serialize5.d)

@[1-5](Begins the same as our other functions)
@[6-7](use NORMAL foreach?)
@[8-12](skip members if they are optional and not present)
@[13-17](skip members if some predicate says we should)
@[18-22](custom function to proceess the data from the row)
@[23](Otherwise, we are going to read it normally, but...)
@[24-30](Use the correct type for reading)
@[31-35](Check for an override of the name in the DB)
@[36-37](Do the actual read)
@[41-42](After the loop, return the result)

---

## Generative vs. Manual

@ul
- 41 lines (Generative) vs. 21 (Manual)
- Generative version DRYer than Manual
- Generative version does not need updating when struct updates
- Generative tends to compile if correct, manual bugs are more subtle.
@ulend

---

## Mixins and CTFE

@ul
- String manupulation in D is strong, and mostly available for CTFE
- You truly can generate code!
- More difficult to follow
- More difficult to debug
- Allows for modular usage
@ulend

---

# Other languages

---

## Java

@ul
- Statically compiled
- Supports generics, but not templates
- Strong Runtime Type Info
@ulend

---

```Java
public interface Serializable {}
```

@ul
- Java uses reflection for introspection
- Serializable interface is just a tag
- There are some hooks, but not statically defined.
- Optimization must rely on JIT
@ulend

---

## C/C++

@ul
- Generative template capabilities
- But not as useful introspection capabilities!
- No good way to loop through members
@ulend

---

@snap[north span-100]

### Traditional serialization

@snapend

@snap[middle span-100]

@code[c++ zoom-09](cpparchive/archive1.cpp)

@snapend

---

@snap[north span-100]
### Traditional serialization
@snapend

@snap[west span-55]
@code[c++ code-max zoom-06](cpparchive/archive1.cpp)
@snapend

@snap[east span-40 text-07]
@ul
- Abstracts what the archiver does
- Focused more on supporting multiple formats than eliminating boilerplate
- You still define how to serialize all the members
@ulend
@snapend

---
## What about C?

@ul
- No introspection capabilities, but has preprocessor!
- Most projects focused on serialization format, not ease of use
@ulend

---

# IOPipe and JSON Serialization

---

Usage for tokenizer

@code[d](jiotest/source/app.d)

@[1-5](Declare a message (input iopipe))
@[6](Create a tokenizer for that iopipe)
@[5,7-8](The object starts!)
@[5,9-13](First member and colon)
@[5,14-16](The first member's value)
@[17-18](And we can continue parsing data)

---

## Deserialization

@ul
- Use template overloading to select how to deserialize
- **Primitive types**: easy, just use `to`
- **Arrays**: loop and recurse
- **Objects**: loop and recurse, using D introspection techniques!
@ulend

---
@code[d code-max zoom-07 code-reveal-fast](jiotest/source/serialize.d)

@[12-19,zoom-12](Some UDA definitions for specialized behavior)
@[184-191,zoom-12](Handle "Roll-your-own")
@[193-199,zoom-12](Function to handle standard structs)
@[200-208,zoom-12](Check for structs that are parsed into one member)
@[209-212,zoom-12](Otherwise, get the list of members we will serialize)
@[176-184,zoom-12](Filter out ignored members)
@[213-219,zoom-12](Assume optionals are visited already)
@[221-230,zoom-12](Special case no members)
@[231-242,zoom-12](Standard parsing fare)
@[243-258,zoom-12](The generative part)
@[263-265,zoom-12](Verify all non optional members were found)
---

# Vibe.d

---

## Exposing a D API on the web

@ul
- Use all available information
- Map names intuitively
- Where custom behavior is needed, use UDAs
@ulend

---

Encapsulate the API in a class

@code[d code-max zoom-08](vibedtest/source/webhandler.d)

@[4-5](Declare the class to handle route '/wh')
@[7](Data storage for the example)
@[9-13](Simple get route)
@[15-20](Store an item)
@[22-31](Retrieve an item)

---
Add authentication

@code[d code-max zoom-08](vibedtest/source/webhandler2.d)

@[4-15](The authorization state)
@[17-20](Now, our class requires authorization)
@[50-58](And the class defines how to get authentication)
@[23-24](Listing the items doesn't require any auth)
@[30-32](But setting an item does)
@[38-40](`_name` is passed to `authinfo.isViewer`)

---

# Demo of vibe.d

---

# Fixing the drawbacks

---

## Compile time is important

@ul
- template and generative programming are compile-time killers.
- vibe.d default installation takes 4.5 seconds to compile on my 2014 Mac.
- CTFE and templates are suspects in compile time bloaters.
@ulend

---

## Avoid the boilerplate

@ul
- Any time you can avoid boilerplate, you should.
- Have the compiler write your boilerplate, it's better at it.
- Generative programming is the crown jewel of D!
@ulend

Note:

- People are bad at boilerplate. It's boring. It's error prone. It's easy to get wrong.
- Compilers are much better at it, especially because they know everything about your program.
- Any barriers that are in place to prevent using the D compiler to generate code should be torn down. I mean syntax ease, compile times, readability, error messages. This is why I keep using D, and enjoying it! We should make this experience the best that can be had.

---

# You have a D compiler, use it!

---

# D Powering Devices

---

### What if D could run on drones?

@ul
- Dconf 2014 - Michael V. Franklin "Tiny Ubiquitous Machines Powered by D"
- Used spec sheet text directly to create register maps
- Well, almost directly
@ulend

Note:
(After all bullets) I have experience in microcontroller programming, working with small 8-bit controllers that had 8k of code space, and 256 BYTES of ram. I've read these spec sheets before, and had to follow every minute detail, so I understand the power of being able to build code directly from the spec sheet.

---

### Use templates to generate perfect boiler plate register maps

*library: https://github.com/JinShil/memory_mapped_io*

@img[clean-img span-60](assets/img/franklin.png)

---

### Use templates to generate perfect boiler plate register maps

@ul
- Enforces all reads and writes are done with as few instructions as possible, and with the correct access type.
- Generated access was 6.5x faster than the chip supplier's C library call
- Once you map the spec to code, ALL the boilerplate is solved for every register
- 1700 page spec
@ulend

---

### Chip spec can be used as code?

@ul
- Imagine if you can simply import the spec, and process it.
- The spec becomes the code, transpiled by the compiler at compile time to the most efficient register mapping possible
- No more discrepencies between code libraries and spec!
@ulend

Note:

This is an incredible and surprising use case for D, given that most of the focus of D is on intel PCs. The potential here to reduce register programming to simply having the compiler reading the spec, and "doing what you would do" is an awsome use of D's generative power.

