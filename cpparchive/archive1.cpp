class Archiveable {
    int x;
    template<class Archive>
    void save(Archive & ar, const unsigned int version) const {
        ar  & x;
    }
    template<class Archive>
    void load(Archive & ar, const unsigned int version) {
        ar  & x;
    }
};
