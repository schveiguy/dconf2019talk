import iopipe.json.parser;

void main()
{
    auto message = `{"id" : 1, "name": "Steve"}`;
    auto parser = message.jsonTokenizer!(false);
    auto jsonItem = parser.next;
    assert(jsonItem.token == JSONToken.ObjectStart);
    jsonItem = parser.next;
    assert(jsonItem.token == JSONToken.String);
    assert(jsonItem.data(parser.chain) == "id");
    jsonItem = parser.next;
    assert(jsonItem.token == JSONToken.Colon);
    jsonItem = parser.next;
    assert(jsonItem.token == JSONToken.Number);
    assert(jsonItem.data(parser.chain) == "1");
    // and so on...
}
