import vibe.vibe;
import vibe.web.auth;

@path("/wh")
class WebHandler
{
    string[string] items;

    void getIndex(HTTPServerResponse res)
    {
        auto items = this.items;
        res.render!("index.dt", items);
    }

    void postItem(HTTPServerRequest req, string name, string value,
                  HTTPServerResponse res)
    {
        items[name] = value;
        res.redirect("/wh/index");
    }

    @path("item/:name")
    void getItem(string _name, HTTPServerResponse res)
    {
        if(auto v = _name in items)
            res.writeBody("<html><body>Item named " ~ _name ~ " is "
                          ~ *v ~ "<br><a href='/wh/index'>Home</a></body></html>",
                          "text/html");
        else
            res.redirect("/wh/index");
    }
}
