import vibe.vibe;
import webhandler;
import webhandler2;

void main()
{
	auto settings = new HTTPServerSettings;
	settings.port = 8080;
	settings.bindAddresses = ["::1", "127.0.0.1"];
        setLogLevel(LogLevel.trace);
        auto router = new URLRouter;
        router.registerWebInterface(new WebHandler);
        router.registerWebInterface(new WebHandler2);
	listenHTTP(settings, router);

	logInfo("Please open http://127.0.0.1:8080/ in your browser.");
	runApplication();
}
