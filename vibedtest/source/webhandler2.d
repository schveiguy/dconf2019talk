import vibe.vibe;
import vibe.web.auth;

struct AuthInfo
{
    string username;
@safe:
    bool isEditor() {
        return username == "root";
    }

    bool isViewer(string _name) {
        return _name.length > 0 && _name[0] == 'a';
    }
}

@path("/wh2")
@requiresAuth
class WebHandler2
{
    string[string] items;

    @noAuth
    void getIndex(HTTPServerResponse res)
    {
        auto items = this.items;
        res.render!("index.dt", items);
    }

    @auth(Role.editor)
    void postItem(HTTPServerRequest req, string name, string value,
                  HTTPServerResponse res)
    {
        items[name] = value;
        res.redirect("/wh2/index");
    }

    @auth(Role.viewer)
    @path("item/:name")
    void getItem(string _name, HTTPServerResponse res)
    {
        if(auto v = _name in items)
            res.writeBody("<html><body>Item named " ~ _name ~ " is "
                          ~ *v ~ "<br><a href='/wh2/index'>Home</a></body></html>",
                          "text/html");
        else
            res.redirect("/wh2/index");
    }

    @safe @noRoute AuthInfo
        authenticate(scope HTTPServerRequest req,
                     scope HTTPServerResponse res)
    {
        // terrible authentication mechanism
        AuthInfo result;
        result.username = req.form.get("username", "anonymous");
        return result;
    }
}
